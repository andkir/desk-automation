import io
import time
import os
import configparser

MAX_SLEEP_DURATION_MS = 20_000


def is_raspberrypi():
    try:
        with io.open('/sys/firmware/devicetree/base/model', 'r') as m:
            if 'raspberry pi' in m.read().lower(): return True
    except Exception: pass
    return False

class UART:
    class MockSerial:
        @staticmethod
        def read(*args):
            print(f"reading serial: {*args,}")
        
        @staticmethod
        def inWaiting(*args):
            print(f"reading remaining data: {*args,}")

    def __init__(self):
        if is_raspberrypi():
            import serial
            ser = serial.Serial("/dev/ttyS0", 9600)
        else:
            print("mocking serial interface")
            ser = self.MockSerial()
        self.serial = ser

    def __read_frame(self) -> str:
        data = self.serial.read()
        time.sleep(0.03)
        data_rem = self.serial.inWaiting()
        data += self.serial.read(data_rem) # read remaining data
        return data

    def read_position(self) -> int:
        print(f"read frame: {self.__read_frame()}")

class GPIO:
    class MockSubprocess:
        @staticmethod
        def run(*args):
            print(f"running os subcommand: {*args,}")

    # GPIO pins used for the up/down buttons
    UP_DIR_PIN = 0
    DOWN_DIR_PIN = 1

    def __init__(self):
        if is_raspberrypi():
            import subprocess as sp
        else:
            print("mocking subprocess interface")
            sp = self.MockSubprocess()
        self.sp = sp
        self.lin = UART()

    def __set_out_mode(self, pin: int):
        self.sp.run(["gpio", "mode", str(pin), "out"])

    def __set_pin(self, pin: int, state: int):
        self.sp.run(["gpio", "write", str(pin), str(state)])

    def __stop(self):
        self.__set_pin(self.UP_DIR_PIN, 0)
        self.__set_pin(self.DOWN_DIR_PIN, 0)

    def up(self, duration_ms: int):
        self.__set_out_mode(self.UP_DIR_PIN)
        self.__set_pin(self.UP_DIR_PIN, 1)
        print(f"sleeping {duration_ms:,} ms")
        time.sleep(duration_ms / 1000.0)
        self.__stop()

    def down(self, duration_ms: int):
        self.__set_out_mode(self.DOWN_DIR_PIN)
        self.__set_pin(self.DOWN_DIR_PIN, 1)
        print(f"sleeping {duration_ms:,} ms")
        time.sleep(duration_ms / 1000.0)
        self.__stop()

CONFIG_FILE = "desk.ini"
CONFIG_SECTION = "main"
CONFIG_KEY = "vpos"

class Desk:
    def __init__(self, gpio: GPIO, force_calibrate: bool = False, config_file = CONFIG_FILE):
        self.gpio = gpio

        self.config = configparser.ConfigParser()
        self.config_file = config_file

        self.vpos = self.get_or_calibrate_vpos(force_calibrate)
        self.update_vpos(self.vpos)

    def get_or_calibrate_vpos(self, force_calibrate) -> int:
        self.config.read(self.config_file)

        if (os.path.exists(self.config_file) and not force_calibrate):
            vpos = self.config.getint(CONFIG_SECTION, CONFIG_KEY)
            print(f'retrieved key from config [vpos: {vpos}]')
            return vpos
        else: 
            print(f'{self.config_file} not found')

            try:
                self.config.add_section(CONFIG_SECTION)
            except configparser.DuplicateSectionError: pass
            
            return self.recalibrate()

    def recalibrate(self) -> int:
        print('recalibrating desk')
        self.gpio.down(MAX_SLEEP_DURATION_MS)

        return 0 # return bottom position

    def update_vpos(self, vpos: int) -> int:
        self.config.set(CONFIG_SECTION, CONFIG_KEY, str(max(vpos, 0)))

        with open(self.config_file, 'w+') as f:
            print(f"updating {self.config_file} with [vpos: {vpos}]")
            self.config.write(f)
        
        self.vpos = vpos
        return self.vpos

    def up(self, duration_ms: int) -> int:
        print(f"moving desk up {duration_ms:,} ms")
        self.gpio.up(duration_ms)

        # adjust and return vpos
        return self.update_vpos(self.vpos + duration_ms)

    def down(self, duration_ms: int) -> int:
        print(f"moving desk down {duration_ms:,} ms")
        self.gpio.down(duration_ms)

        # adjust and return vpos
        return self.update_vpos(self.vpos - duration_ms)

    # def run_test_cycle(self):
    #     print("runnig test cycle")
    #     self.up(MAX_SLEEP_DURATION_MS)
    #     print("returning desk to bottom")
    #     self.down(MAX_SLEEP_DURATION_MS)
    #     print("done running testing cycle")

def is_int(data: str) -> bool:
    try:
        int(data)
        return True
    except ValueError:
        return False


if __name__ == "__main__":
    gpio = GPIO()
    desk = Desk(gpio, force_calibrate=True)

    print(f"current desk position {desk.vpos}")

    while True:
        data = input("program input: ")

        try:
            direction, duration = data[:1], data[1:]

            if (direction == "d"):
                desk.down(int(duration))
            elif (direction == "u"):
                desk.up(int(duration))
            else:
                raise ValueError(f"{direction} is not a valid direction, valid options are d/u")
        except:
            print("failed to parse input, needs to be both a direction and a number")
